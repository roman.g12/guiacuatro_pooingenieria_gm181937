﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo2
{
    public class Estudiante : Usuario
    {
        public string Nombre { get => this.nombre; set => this.nombre = value; }
        public string User { get => this.user; set => this.user = value; }
        public string Codigo { get => this.codigo; set => this.codigo = value; }
        public Estudiante() : base(){}
        public Estudiante(string nombre, string user, string codigo) : base(nombre, user, codigo){}

    }
}
