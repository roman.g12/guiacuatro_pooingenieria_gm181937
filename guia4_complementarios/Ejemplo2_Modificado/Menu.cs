﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo2
{
    public partial class Menu : Ejemplo2.frmBase
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void btnAlumno_Click(object sender, EventArgs e)
        {
            frmEstudiante frmEstudiante = new frmEstudiante();
            this.Hide();
            frmEstudiante.Show();
        }

        private void btnDocente_Click(object sender, EventArgs e)
        {
            registroDocente registroDocente = new registroDocente();
            this.Hide();
            registroDocente.Show();
        }
    }
}
