﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo2
{
    public partial class frmEstudiante : Ejemplo2.frmRegistro
    {
        public frmEstudiante()
        {
            InitializeComponent();
        }

        private void frmEstudiante_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        List<TextBox> textBoxes = new List<TextBox>();
        List<Estudiante> Estudiantes = new List<Estudiante>();
        private int edit_indice = -1;
        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            try
            {
                Estudiante estudianteTemp = new Estudiante();
                textBoxes.Clear();
                textBoxes.Add(txtCodigo);
                textBoxes.Add(txtNombre);
                textBoxes.Add(txtUsuario);
                if (ControlesVacios(textBoxes) == false)
                {
                    estudianteTemp.Nombre = txtNombre.Text;
                    estudianteTemp.User = txtUsuario.Text;
                    estudianteTemp.Codigo = txtCodigo.Text;
                    Estudiantes.Add(estudianteTemp);
                    LlenarData();
                    MessageBox.Show("Se ha registrado el estudiante", "Registro", MessageBoxButtons.OK,
            MessageBoxIcon.Information);
                }
                else
                {
                    throw new Exception("Hay campos incompletos");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK,
    MessageBoxIcon.Warning);
            }

        }

        private void btnModificar_Click_1(object sender, EventArgs e)
        {
            try
            {
                Estudiante estudianteTemp = new Estudiante();
                textBoxes.Clear();
                textBoxes.Add(txtCodigo);
                textBoxes.Add(txtNombre);
                textBoxes.Add(txtUsuario);
                if (ControlesVacios(textBoxes) == false)
                {
                    estudianteTemp.Nombre = txtNombre.Text;
                    estudianteTemp.User = txtUsuario.Text;
                    estudianteTemp.Codigo = txtCodigo.Text;
                    int index = dgvDatos1.Rows.Add();
                    Estudiantes.Add(estudianteTemp);
                    LlenarData();
                    btnGuardar.Enabled = true;
                    MessageBox.Show("Se ha modifcado el estudiante", "Registro", MessageBoxButtons.OK,
            MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK,
    MessageBoxIcon.Warning);
            }

        }
        public bool ControlesVacios(List<TextBox> textBoxes)
        {
            foreach (TextBox t in textBoxes)
            {
                if (String.IsNullOrEmpty(t.Text))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public void LlenarData()
        {
            dgvDatos1.DataSource = null;
            dgvDatos1.DataSource = Estudiantes;
        }

        private void dgvDatos1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow selected = dgvDatos1.SelectedRows[0];
            int posicion = dgvDatos1.Rows.IndexOf(selected); //almacena en cual fila estoy
            edit_indice = posicion;
            Estudiante estudiante = Estudiantes[posicion];
            txtCodigo.Text = estudiante.Codigo;
            txtNombre.Text = estudiante.Nombre;
            txtUsuario.Text = estudiante.User;
            btnGuardar.Enabled = false;
            btnModificar.Enabled = true;
        }
    }
}
