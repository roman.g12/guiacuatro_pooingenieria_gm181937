﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo2
{
    public partial class registroDocente : Ejemplo2.frmRegistro
    {
        public registroDocente()
        {
            InitializeComponent();
        }

        private void registroDocente_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
        List<TextBox> textBoxes = new List<TextBox>();
        List<Docente> Docentes = new List<Docente>();
        private int edit_indice = -1;
        private void btnGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                Docente docenteTemp = new Docente();
                textBoxes.Clear();
                textBoxes.Add(txtCodigo);
                textBoxes.Add(txtNombre);
                textBoxes.Add(txtUsuario);
                textBoxes.Add(txtMateria);
                if (ControlesVacios(textBoxes) == false)
                {
                    docenteTemp.Nombre = txtNombre.Text;
                    docenteTemp.User = txtUsuario.Text;
                    docenteTemp.Codigo = txtCodigo.Text;
                    docenteTemp.Materia = txtMateria.Text;
                    Docentes.Add(docenteTemp);
                    LlenarData();
                    MessageBox.Show("Se ha registrado el docente", "Registro", MessageBoxButtons.OK,
            MessageBoxIcon.Information);
                }
                else
                {
                    throw new Exception("Hay campos incompletos");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK,
    MessageBoxIcon.Warning);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {

            try
            {
                Docente docenteTemp = new Docente();
                textBoxes.Clear();
                textBoxes.Add(txtCodigo);
                textBoxes.Add(txtNombre);
                textBoxes.Add(txtUsuario);
                textBoxes.Add(txtMateria);
                if (ControlesVacios(textBoxes) == false)
                {
                    docenteTemp.Nombre = txtNombre.Text;
                    docenteTemp.User = txtUsuario.Text;
                    docenteTemp.Codigo = txtCodigo.Text;
                    docenteTemp.Materia = txtMateria.Text;
                    Docentes.Add(docenteTemp);
                    LlenarData();
                    btnGuardar.Enabled = true;
                    MessageBox.Show("Se ha modificado el docente", "Registro", MessageBoxButtons.OK,
            MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Advertencia", MessageBoxButtons.OK,
    MessageBoxIcon.Warning);
            }
        }

        private void btnLimpiar_Click_1(object sender, EventArgs e)
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtUsuario.Clear();
            txtMateria.Clear();
        }

        private void dgvDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow selected = dgvDatos.SelectedRows[0];
            int posicion = dgvDatos.Rows.IndexOf(selected); //almacena en cual fila estoy
            edit_indice = posicion;
            Docente docente = Docentes[posicion];
            txtCodigo.Text = docente.Codigo;
            txtNombre.Text = docente.Nombre;
            txtUsuario.Text = docente.User;
            txtMateria.Text = docente.Materia;
            btnGuardar.Enabled = false;
            btnModificar.Enabled = true;
        }
        public bool ControlesVacios(List<TextBox> textBoxes)
        {
            foreach (TextBox t in textBoxes)
            {
                if (String.IsNullOrEmpty(t.Text))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        public void LlenarData()
        {
            dgvDatos.DataSource = null;
            dgvDatos.DataSource = Docentes;
        }
    }
}
