﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Ejemplo2
{
    public partial class frmRegistro : Ejemplo2.frmBase
    {
        public frmRegistro()
        {
            InitializeComponent();
        }

        public virtual void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtUsuario.Clear();
        }
    }
}
