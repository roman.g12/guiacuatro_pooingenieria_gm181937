﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo2
{
    public class Docente : Usuario
    {
        private string materia;
        public string Nombre { get => this.nombre; set => this.nombre = value; }
        public string User { get => this.user; set => this.user = value; }
        public string Codigo { get => this.codigo; set => this.codigo = value; }
        public string Materia { get => materia; set => materia = value; }

        public Docente() : base(){}
        public Docente(string nombre, string user, string codigo) : base(nombre,user,codigo){}
    }
}
