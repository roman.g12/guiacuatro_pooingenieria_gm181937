﻿namespace Ejemplo2
{
    partial class Menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDocente = new System.Windows.Forms.Button();
            this.btnAlumno = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDocente
            // 
            this.btnDocente.BackColor = System.Drawing.Color.Black;
            this.btnDocente.ForeColor = System.Drawing.Color.White;
            this.btnDocente.Location = new System.Drawing.Point(400, 78);
            this.btnDocente.Name = "btnDocente";
            this.btnDocente.Size = new System.Drawing.Size(150, 81);
            this.btnDocente.TabIndex = 21;
            this.btnDocente.Text = "Registro Docentes";
            this.btnDocente.UseVisualStyleBackColor = false;
            this.btnDocente.Click += new System.EventHandler(this.btnDocente_Click);
            // 
            // btnAlumno
            // 
            this.btnAlumno.BackColor = System.Drawing.Color.Black;
            this.btnAlumno.ForeColor = System.Drawing.Color.White;
            this.btnAlumno.Location = new System.Drawing.Point(132, 78);
            this.btnAlumno.Name = "btnAlumno";
            this.btnAlumno.Size = new System.Drawing.Size(150, 81);
            this.btnAlumno.TabIndex = 22;
            this.btnAlumno.Text = "Registro Docentes";
            this.btnAlumno.UseVisualStyleBackColor = false;
            this.btnAlumno.Click += new System.EventHandler(this.btnAlumno_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.ClientSize = new System.Drawing.Size(815, 215);
            this.Controls.Add(this.btnAlumno);
            this.Controls.Add(this.btnDocente);
            this.Name = "Menu";
            this.Controls.SetChildIndex(this.btnDocente, 0);
            this.Controls.SetChildIndex(this.btnAlumno, 0);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDocente;
        private System.Windows.Forms.Button btnAlumno;
    }
}
