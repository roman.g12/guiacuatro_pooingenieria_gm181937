﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo2
{
    public class Usuario
    {
        //ATRIBUTOS
        protected string nombre;
        protected string user;
        protected string codigo;

        public Usuario(string nombre, string usuario, string codigo)
        {
            this.nombre = nombre;
            this.user = usuario;
            this.codigo = codigo;
        }
        public Usuario()
        {
        }
    }
}
