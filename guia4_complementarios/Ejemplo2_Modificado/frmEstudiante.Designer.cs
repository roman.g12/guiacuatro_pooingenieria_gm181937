﻿namespace Ejemplo2
{
    partial class frmEstudiante
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.dgvDatos1 = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGuardar
            // 
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click_1);
            // 
            // btnModificar
            // 
            this.btnModificar.Enabled = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(180, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(347, 33);
            this.label4.TabIndex = 25;
            this.label4.Text = "Registro de Estudiantes";
            // 
            // dgvDatos1
            // 
            this.dgvDatos1.AllowUserToAddRows = false;
            this.dgvDatos1.AllowUserToDeleteRows = false;
            this.dgvDatos1.BackgroundColor = System.Drawing.Color.Gray;
            this.dgvDatos1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatos1.GridColor = System.Drawing.Color.Black;
            this.dgvDatos1.Location = new System.Drawing.Point(12, 396);
            this.dgvDatos1.Name = "dgvDatos1";
            this.dgvDatos1.ReadOnly = true;
            this.dgvDatos1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDatos1.Size = new System.Drawing.Size(758, 213);
            this.dgvDatos1.TabIndex = 26;
            this.dgvDatos1.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDatos1_CellDoubleClick);
            // 
            // frmEstudiante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.ClientSize = new System.Drawing.Size(789, 629);
            this.Controls.Add(this.dgvDatos1);
            this.Controls.Add(this.label4);
            this.Name = "frmEstudiante";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmEstudiante_FormClosed);
            this.Controls.SetChildIndex(this.btnLimpiar, 0);
            this.Controls.SetChildIndex(this.btnModificar, 0);
            this.Controls.SetChildIndex(this.btnGuardar, 0);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.label4, 0);
            this.Controls.SetChildIndex(this.dgvDatos1, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatos1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.DataGridView dgvDatos1;
    }
}
