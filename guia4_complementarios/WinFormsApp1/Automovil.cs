﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public abstract class Automovil
    {
        private double year;

        public Automovil(double year)
        {
            this.year = year;
        }

        //Método virtual para mostrar los resultados en los ListBox
        public virtual void Resultados(ListBox lst)
        {

        }

        public double Year { get => year; set => year = value; }
    }
}
