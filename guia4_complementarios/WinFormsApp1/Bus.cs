﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public class Bus : Automovil
    {
        private string marca;
        private string ruta;

        public Bus(double A, string marca, string ruta) : base(A)
        {
            this.marca = marca;
            this.ruta = ruta;
        }

        public override void Resultados(ListBox lst)
        {
            Year = Year;
            Marca = Marca;
            Ruta = Ruta;

            lst.Items.Clear();
            lst.Items.Add("Año: " + Year);
            lst.Items.Add("Marca: " + Marca);
            lst.Items.Add("Ruta: " + Ruta);
        }

        public string Marca { get => marca; set => marca = value; }
        public string Ruta { get => ruta; set => ruta = value; }
    }
}
