﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void lblMostrarMoto_Click(object sender, EventArgs e)
        {
            int year = Int32.Parse(txtYearMoto.Text);
            string marca = txtMarcaMoto.Text;
            string color = txtColorCarro.Text;

            Moto moto = new Moto(year, marca, color);

            moto.Resultados(lstResultadosMoto);
        }

        private void btnMostrarCarro_Click(object sender, EventArgs e)
        {
            int year = Int32.Parse(txtYearCarro.Text);
            string marca = txtMarcaCarro.Text;
            string color = txtColorCarro.Text;
            double kilometraje = double.Parse(txtKilometraje.Text);
            string modelo = txtModeloCarro.Text;

            Carro carro = new Carro(year, marca, modelo, color, kilometraje);

            carro.Resultados(lstMostrarCarro);
        }

        private void btnMostrarBus_Click(object sender, EventArgs e)
        {
            int year = Int32.Parse(txtYearBus.Text);
            string marca = txtMarcaBus.Text;
            string ruta = txtRutaBus.Text;

            Bus bus = new Bus(year, marca, ruta);

            bus.Resultados(lstMostrarBus);
        }
    }
}
