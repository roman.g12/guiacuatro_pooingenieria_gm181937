﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public class Moto : Automovil
    {
        private string color;
        private string marca;

        public override void Resultados(ListBox lst)
        {
            Year = Year;
            Color = Color;
            Marca = Marca;

            lst.Items.Clear();
            lst.Items.Add("Año: " + Year);
            lst.Items.Add("Marca: " + Marca);
            lst.Items.Add("Color: " + Color);
        }

        public Moto(double A, string marca, string color) : base(A)
        {
            this.marca = marca;
            this.color = color;
        }

        public string Color { get => color; set => color = value; }
        public string Marca { get => marca; set => marca = value; }
    }
}
