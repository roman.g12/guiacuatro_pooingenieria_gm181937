﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace WinFormsApp1
{
    public class Carro : Automovil
    {
        private string marca;
        private string modelo;
        private string color;
        private double kilometraje;

        public Carro(double A, string marca, string modelo, string color, double kilometraje) : base(A)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.kilometraje = kilometraje;
            this.color = color;
        }

        public override void Resultados(ListBox lst)
        {
            Year = Year;
            Color = Color;
            Marca = Marca;
            Modelo = Modelo;
            Kilometraje = Kilometraje;

            lst.Items.Clear();
            lst.Items.Add("Año: " + Year);
            lst.Items.Add("Marca: " + Marca);
            lst.Items.Add("Color: " + Color);
            lst.Items.Add("Modelo: " + Modelo);
            lst.Items.Add("Kilometraje: " + Kilometraje);
        }

        public string Marca { get => marca; set => marca = value; }
        public string Modelo { get => modelo; set => modelo = value; }
        public string Color { get => color; set => color = value; }
        public double Kilometraje { get => kilometraje; set => kilometraje = value; }
    }
}
