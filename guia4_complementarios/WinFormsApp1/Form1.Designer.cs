﻿
namespace WinFormsApp1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcContenedor = new System.Windows.Forms.TabControl();
            this.tbpMoto = new System.Windows.Forms.TabPage();
            this.lstResultadosMoto = new System.Windows.Forms.ListBox();
            this.txtColorMoto = new System.Windows.Forms.TextBox();
            this.txtMarcaMoto = new System.Windows.Forms.TextBox();
            this.txtYearMoto = new System.Windows.Forms.TextBox();
            this.lblMostrarMoto = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMarcaMoto = new System.Windows.Forms.Label();
            this.lblYearMoto = new System.Windows.Forms.Label();
            this.tbpCarro = new System.Windows.Forms.TabPage();
            this.txtModeloCarro = new System.Windows.Forms.TextBox();
            this.lblModeloCarro = new System.Windows.Forms.Label();
            this.txtKilometraje = new System.Windows.Forms.TextBox();
            this.lblKilometrajeCarro = new System.Windows.Forms.Label();
            this.lstMostrarCarro = new System.Windows.Forms.ListBox();
            this.txtColorCarro = new System.Windows.Forms.TextBox();
            this.txtMarcaCarro = new System.Windows.Forms.TextBox();
            this.txtYearCarro = new System.Windows.Forms.TextBox();
            this.btnMostrarCarro = new System.Windows.Forms.Button();
            this.lblColorCarro = new System.Windows.Forms.Label();
            this.lblMarcaCarro = new System.Windows.Forms.Label();
            this.lblYearCarro = new System.Windows.Forms.Label();
            this.tbpBus = new System.Windows.Forms.TabPage();
            this.txtRutaBus = new System.Windows.Forms.TextBox();
            this.lblRuta = new System.Windows.Forms.Label();
            this.lstMostrarBus = new System.Windows.Forms.ListBox();
            this.txtMarcaBus = new System.Windows.Forms.TextBox();
            this.txtYearBus = new System.Windows.Forms.TextBox();
            this.btnMostrarBus = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbcContenedor.SuspendLayout();
            this.tbpMoto.SuspendLayout();
            this.tbpCarro.SuspendLayout();
            this.tbpBus.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbcContenedor
            // 
            this.tbcContenedor.Controls.Add(this.tbpMoto);
            this.tbcContenedor.Controls.Add(this.tbpCarro);
            this.tbcContenedor.Controls.Add(this.tbpBus);
            this.tbcContenedor.Location = new System.Drawing.Point(12, 12);
            this.tbcContenedor.Name = "tbcContenedor";
            this.tbcContenedor.SelectedIndex = 0;
            this.tbcContenedor.Size = new System.Drawing.Size(627, 426);
            this.tbcContenedor.TabIndex = 0;
            // 
            // tbpMoto
            // 
            this.tbpMoto.Controls.Add(this.lstResultadosMoto);
            this.tbpMoto.Controls.Add(this.txtColorMoto);
            this.tbpMoto.Controls.Add(this.txtMarcaMoto);
            this.tbpMoto.Controls.Add(this.txtYearMoto);
            this.tbpMoto.Controls.Add(this.lblMostrarMoto);
            this.tbpMoto.Controls.Add(this.label1);
            this.tbpMoto.Controls.Add(this.lblMarcaMoto);
            this.tbpMoto.Controls.Add(this.lblYearMoto);
            this.tbpMoto.Location = new System.Drawing.Point(4, 24);
            this.tbpMoto.Name = "tbpMoto";
            this.tbpMoto.Padding = new System.Windows.Forms.Padding(3);
            this.tbpMoto.Size = new System.Drawing.Size(619, 398);
            this.tbpMoto.TabIndex = 0;
            this.tbpMoto.Text = "Moto";
            this.tbpMoto.UseVisualStyleBackColor = true;
            // 
            // lstResultadosMoto
            // 
            this.lstResultadosMoto.FormattingEnabled = true;
            this.lstResultadosMoto.ItemHeight = 15;
            this.lstResultadosMoto.Location = new System.Drawing.Point(381, 41);
            this.lstResultadosMoto.Name = "lstResultadosMoto";
            this.lstResultadosMoto.Size = new System.Drawing.Size(193, 229);
            this.lstResultadosMoto.TabIndex = 7;
            // 
            // txtColorMoto
            // 
            this.txtColorMoto.Location = new System.Drawing.Point(101, 159);
            this.txtColorMoto.Name = "txtColorMoto";
            this.txtColorMoto.Size = new System.Drawing.Size(131, 23);
            this.txtColorMoto.TabIndex = 6;
            // 
            // txtMarcaMoto
            // 
            this.txtMarcaMoto.Location = new System.Drawing.Point(101, 99);
            this.txtMarcaMoto.Name = "txtMarcaMoto";
            this.txtMarcaMoto.Size = new System.Drawing.Size(131, 23);
            this.txtMarcaMoto.TabIndex = 5;
            // 
            // txtYearMoto
            // 
            this.txtYearMoto.Location = new System.Drawing.Point(101, 38);
            this.txtYearMoto.Name = "txtYearMoto";
            this.txtYearMoto.Size = new System.Drawing.Size(131, 23);
            this.txtYearMoto.TabIndex = 4;
            // 
            // lblMostrarMoto
            // 
            this.lblMostrarMoto.Location = new System.Drawing.Point(24, 238);
            this.lblMostrarMoto.Name = "lblMostrarMoto";
            this.lblMostrarMoto.Size = new System.Drawing.Size(88, 34);
            this.lblMostrarMoto.TabIndex = 3;
            this.lblMostrarMoto.Text = "Mostrar";
            this.lblMostrarMoto.UseVisualStyleBackColor = true;
            this.lblMostrarMoto.Click += new System.EventHandler(this.lblMostrarMoto_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "Color";
            // 
            // lblMarcaMoto
            // 
            this.lblMarcaMoto.AutoSize = true;
            this.lblMarcaMoto.Location = new System.Drawing.Point(24, 102);
            this.lblMarcaMoto.Name = "lblMarcaMoto";
            this.lblMarcaMoto.Size = new System.Drawing.Size(43, 15);
            this.lblMarcaMoto.TabIndex = 1;
            this.lblMarcaMoto.Text = "Marca:";
            // 
            // lblYearMoto
            // 
            this.lblYearMoto.AutoSize = true;
            this.lblYearMoto.Location = new System.Drawing.Point(24, 41);
            this.lblYearMoto.Name = "lblYearMoto";
            this.lblYearMoto.Size = new System.Drawing.Size(32, 15);
            this.lblYearMoto.TabIndex = 0;
            this.lblYearMoto.Text = "Año:";
            // 
            // tbpCarro
            // 
            this.tbpCarro.Controls.Add(this.txtModeloCarro);
            this.tbpCarro.Controls.Add(this.lblModeloCarro);
            this.tbpCarro.Controls.Add(this.txtKilometraje);
            this.tbpCarro.Controls.Add(this.lblKilometrajeCarro);
            this.tbpCarro.Controls.Add(this.lstMostrarCarro);
            this.tbpCarro.Controls.Add(this.txtColorCarro);
            this.tbpCarro.Controls.Add(this.txtMarcaCarro);
            this.tbpCarro.Controls.Add(this.txtYearCarro);
            this.tbpCarro.Controls.Add(this.btnMostrarCarro);
            this.tbpCarro.Controls.Add(this.lblColorCarro);
            this.tbpCarro.Controls.Add(this.lblMarcaCarro);
            this.tbpCarro.Controls.Add(this.lblYearCarro);
            this.tbpCarro.Location = new System.Drawing.Point(4, 24);
            this.tbpCarro.Name = "tbpCarro";
            this.tbpCarro.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCarro.Size = new System.Drawing.Size(619, 398);
            this.tbpCarro.TabIndex = 1;
            this.tbpCarro.Text = "Carro";
            this.tbpCarro.UseVisualStyleBackColor = true;
            // 
            // txtModeloCarro
            // 
            this.txtModeloCarro.Location = new System.Drawing.Point(102, 284);
            this.txtModeloCarro.Name = "txtModeloCarro";
            this.txtModeloCarro.Size = new System.Drawing.Size(131, 23);
            this.txtModeloCarro.TabIndex = 19;
            // 
            // lblModeloCarro
            // 
            this.lblModeloCarro.AutoSize = true;
            this.lblModeloCarro.Location = new System.Drawing.Point(25, 287);
            this.lblModeloCarro.Name = "lblModeloCarro";
            this.lblModeloCarro.Size = new System.Drawing.Size(51, 15);
            this.lblModeloCarro.TabIndex = 18;
            this.lblModeloCarro.Text = "Modelo:";
            // 
            // txtKilometraje
            // 
            this.txtKilometraje.Location = new System.Drawing.Point(102, 218);
            this.txtKilometraje.Name = "txtKilometraje";
            this.txtKilometraje.Size = new System.Drawing.Size(131, 23);
            this.txtKilometraje.TabIndex = 17;
            // 
            // lblKilometrajeCarro
            // 
            this.lblKilometrajeCarro.AutoSize = true;
            this.lblKilometrajeCarro.Location = new System.Drawing.Point(25, 221);
            this.lblKilometrajeCarro.Name = "lblKilometrajeCarro";
            this.lblKilometrajeCarro.Size = new System.Drawing.Size(70, 15);
            this.lblKilometrajeCarro.TabIndex = 16;
            this.lblKilometrajeCarro.Text = "Kilometraje:";
            // 
            // lstMostrarCarro
            // 
            this.lstMostrarCarro.FormattingEnabled = true;
            this.lstMostrarCarro.ItemHeight = 15;
            this.lstMostrarCarro.Location = new System.Drawing.Point(382, 39);
            this.lstMostrarCarro.Name = "lstMostrarCarro";
            this.lstMostrarCarro.Size = new System.Drawing.Size(193, 229);
            this.lstMostrarCarro.TabIndex = 15;
            // 
            // txtColorCarro
            // 
            this.txtColorCarro.Location = new System.Drawing.Point(102, 157);
            this.txtColorCarro.Name = "txtColorCarro";
            this.txtColorCarro.Size = new System.Drawing.Size(131, 23);
            this.txtColorCarro.TabIndex = 14;
            // 
            // txtMarcaCarro
            // 
            this.txtMarcaCarro.Location = new System.Drawing.Point(102, 97);
            this.txtMarcaCarro.Name = "txtMarcaCarro";
            this.txtMarcaCarro.Size = new System.Drawing.Size(131, 23);
            this.txtMarcaCarro.TabIndex = 13;
            // 
            // txtYearCarro
            // 
            this.txtYearCarro.Location = new System.Drawing.Point(102, 36);
            this.txtYearCarro.Name = "txtYearCarro";
            this.txtYearCarro.Size = new System.Drawing.Size(131, 23);
            this.txtYearCarro.TabIndex = 12;
            // 
            // btnMostrarCarro
            // 
            this.btnMostrarCarro.Location = new System.Drawing.Point(25, 335);
            this.btnMostrarCarro.Name = "btnMostrarCarro";
            this.btnMostrarCarro.Size = new System.Drawing.Size(88, 34);
            this.btnMostrarCarro.TabIndex = 11;
            this.btnMostrarCarro.Text = "Mostrar";
            this.btnMostrarCarro.UseVisualStyleBackColor = true;
            this.btnMostrarCarro.Click += new System.EventHandler(this.btnMostrarCarro_Click);
            // 
            // lblColorCarro
            // 
            this.lblColorCarro.AutoSize = true;
            this.lblColorCarro.Location = new System.Drawing.Point(25, 160);
            this.lblColorCarro.Name = "lblColorCarro";
            this.lblColorCarro.Size = new System.Drawing.Size(36, 15);
            this.lblColorCarro.TabIndex = 10;
            this.lblColorCarro.Text = "Color";
            // 
            // lblMarcaCarro
            // 
            this.lblMarcaCarro.AutoSize = true;
            this.lblMarcaCarro.Location = new System.Drawing.Point(25, 100);
            this.lblMarcaCarro.Name = "lblMarcaCarro";
            this.lblMarcaCarro.Size = new System.Drawing.Size(43, 15);
            this.lblMarcaCarro.TabIndex = 9;
            this.lblMarcaCarro.Text = "Marca:";
            // 
            // lblYearCarro
            // 
            this.lblYearCarro.AutoSize = true;
            this.lblYearCarro.Location = new System.Drawing.Point(25, 39);
            this.lblYearCarro.Name = "lblYearCarro";
            this.lblYearCarro.Size = new System.Drawing.Size(32, 15);
            this.lblYearCarro.TabIndex = 8;
            this.lblYearCarro.Text = "Año:";
            // 
            // tbpBus
            // 
            this.tbpBus.Controls.Add(this.txtRutaBus);
            this.tbpBus.Controls.Add(this.lblRuta);
            this.tbpBus.Controls.Add(this.lstMostrarBus);
            this.tbpBus.Controls.Add(this.txtMarcaBus);
            this.tbpBus.Controls.Add(this.txtYearBus);
            this.tbpBus.Controls.Add(this.btnMostrarBus);
            this.tbpBus.Controls.Add(this.label3);
            this.tbpBus.Controls.Add(this.label4);
            this.tbpBus.Location = new System.Drawing.Point(4, 24);
            this.tbpBus.Name = "tbpBus";
            this.tbpBus.Size = new System.Drawing.Size(619, 398);
            this.tbpBus.TabIndex = 2;
            this.tbpBus.Text = "Bus";
            this.tbpBus.UseVisualStyleBackColor = true;
            // 
            // txtRutaBus
            // 
            this.txtRutaBus.Location = new System.Drawing.Point(101, 171);
            this.txtRutaBus.Name = "txtRutaBus";
            this.txtRutaBus.Size = new System.Drawing.Size(131, 23);
            this.txtRutaBus.TabIndex = 17;
            // 
            // lblRuta
            // 
            this.lblRuta.AutoSize = true;
            this.lblRuta.Location = new System.Drawing.Point(24, 171);
            this.lblRuta.Name = "lblRuta";
            this.lblRuta.Size = new System.Drawing.Size(34, 15);
            this.lblRuta.TabIndex = 16;
            this.lblRuta.Text = "Ruta:";
            // 
            // lstMostrarBus
            // 
            this.lstMostrarBus.FormattingEnabled = true;
            this.lstMostrarBus.ItemHeight = 15;
            this.lstMostrarBus.Location = new System.Drawing.Point(381, 46);
            this.lstMostrarBus.Name = "lstMostrarBus";
            this.lstMostrarBus.Size = new System.Drawing.Size(193, 229);
            this.lstMostrarBus.TabIndex = 15;
            // 
            // txtMarcaBus
            // 
            this.txtMarcaBus.Location = new System.Drawing.Point(101, 104);
            this.txtMarcaBus.Name = "txtMarcaBus";
            this.txtMarcaBus.Size = new System.Drawing.Size(131, 23);
            this.txtMarcaBus.TabIndex = 13;
            // 
            // txtYearBus
            // 
            this.txtYearBus.Location = new System.Drawing.Point(101, 43);
            this.txtYearBus.Name = "txtYearBus";
            this.txtYearBus.Size = new System.Drawing.Size(131, 23);
            this.txtYearBus.TabIndex = 12;
            // 
            // btnMostrarBus
            // 
            this.btnMostrarBus.Location = new System.Drawing.Point(24, 241);
            this.btnMostrarBus.Name = "btnMostrarBus";
            this.btnMostrarBus.Size = new System.Drawing.Size(88, 34);
            this.btnMostrarBus.TabIndex = 11;
            this.btnMostrarBus.Text = "Mostrar";
            this.btnMostrarBus.UseVisualStyleBackColor = true;
            this.btnMostrarBus.Click += new System.EventHandler(this.btnMostrarBus_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "Marca:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Año:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 450);
            this.Controls.Add(this.tbcContenedor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tbcContenedor.ResumeLayout(false);
            this.tbpMoto.ResumeLayout(false);
            this.tbpMoto.PerformLayout();
            this.tbpCarro.ResumeLayout(false);
            this.tbpCarro.PerformLayout();
            this.tbpBus.ResumeLayout(false);
            this.tbpBus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcContenedor;
        private System.Windows.Forms.TabPage tbpMoto;
        private System.Windows.Forms.TextBox txtColorMoto;
        private System.Windows.Forms.TextBox txtMarcaMoto;
        private System.Windows.Forms.TextBox txtYearMoto;
        private System.Windows.Forms.Button lblMostrarMoto;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMarcaMoto;
        private System.Windows.Forms.Label lblYearMoto;
        private System.Windows.Forms.TabPage tbpCarro;
        private System.Windows.Forms.TabPage tbpBus;
        private System.Windows.Forms.ListBox lstResultadosMoto;
        private System.Windows.Forms.TextBox txtKilometraje;
        private System.Windows.Forms.Label lblKilometrajeCarro;
        private System.Windows.Forms.ListBox lstMostrarCarro;
        private System.Windows.Forms.TextBox txtColorCarro;
        private System.Windows.Forms.TextBox txtMarcaCarro;
        private System.Windows.Forms.TextBox txtYearCarro;
        private System.Windows.Forms.Button btnMostrarCarro;
        private System.Windows.Forms.Label lblColorCarro;
        private System.Windows.Forms.Label lblMarcaCarro;
        private System.Windows.Forms.Label lblYearCarro;
        private System.Windows.Forms.TextBox txtModeloCarro;
        private System.Windows.Forms.Label lblModeloCarro;
        private System.Windows.Forms.TextBox txtRutaBus;
        private System.Windows.Forms.Label lblRuta;
        private System.Windows.Forms.ListBox lstMostrarBus;
        private System.Windows.Forms.TextBox txtMarcaBus;
        private System.Windows.Forms.TextBox txtYearBus;
        private System.Windows.Forms.Button btnMostrarBus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

