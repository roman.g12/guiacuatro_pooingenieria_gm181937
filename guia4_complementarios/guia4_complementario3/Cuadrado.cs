﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace guia4_complementario3
{
    public class Cuadrado : Figura
    {
        private double lado;

        public Cuadrado(double A, double L):base(A) //Constructor
        {
            Lado = L;
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (Lado * Lado);
            LR.Text = "Área: " + Area;
        }

        public double Lado { get => lado; set => lado = value; }
    }
}
