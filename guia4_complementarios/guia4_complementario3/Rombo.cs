﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace guia4_complementario3
{
    public class Rombo : Figura
    {
        private double diagonalmayor;
        private double diagonalmenor;

        public Rombo(double A, double mayor, double menor) : base(A)
        {
            diagonalmayor = mayor;
            diagonalmenor = menor;
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (Diagonalmayor * Diagonalmenor) / 2;
            LR.Text = "Area: " + Area;
        }

        public double Diagonalmayor { get => diagonalmayor; set => diagonalmayor = value; }
        public double Diagonalmenor { get => diagonalmenor; set => diagonalmenor = value; }
    }
}
