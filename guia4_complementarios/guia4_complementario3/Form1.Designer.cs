﻿
namespace guia4_complementario3
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbContenedor = new System.Windows.Forms.TabControl();
            this.tbpCuadrado = new System.Windows.Forms.TabPage();
            this.lblRespuestaCuadrado = new System.Windows.Forms.Label();
            this.btnCalcularCuadrado = new System.Windows.Forms.Button();
            this.txtLado = new System.Windows.Forms.TextBox();
            this.lblLado = new System.Windows.Forms.Label();
            this.tbpCirculo = new System.Windows.Forms.TabPage();
            this.btnCalcularCirculo = new System.Windows.Forms.Button();
            this.txtRadio = new System.Windows.Forms.TextBox();
            this.lblRespuestaCirculo = new System.Windows.Forms.Label();
            this.lblRadio = new System.Windows.Forms.Label();
            this.tboRombo = new System.Windows.Forms.TabPage();
            this.btnCalcularRombo = new System.Windows.Forms.Button();
            this.txtDiagonalMayor = new System.Windows.Forms.TextBox();
            this.lblRespuestaRombo = new System.Windows.Forms.Label();
            this.lblDiagonalMayor = new System.Windows.Forms.Label();
            this.lblDiagonalMenor = new System.Windows.Forms.Label();
            this.txtDiagonalMenor = new System.Windows.Forms.TextBox();
            this.tbContenedor.SuspendLayout();
            this.tbpCuadrado.SuspendLayout();
            this.tbpCirculo.SuspendLayout();
            this.tboRombo.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbContenedor
            // 
            this.tbContenedor.Controls.Add(this.tbpCuadrado);
            this.tbContenedor.Controls.Add(this.tbpCirculo);
            this.tbContenedor.Controls.Add(this.tboRombo);
            this.tbContenedor.Location = new System.Drawing.Point(1, 3);
            this.tbContenedor.Name = "tbContenedor";
            this.tbContenedor.SelectedIndex = 0;
            this.tbContenedor.Size = new System.Drawing.Size(592, 409);
            this.tbContenedor.TabIndex = 0;
            // 
            // tbpCuadrado
            // 
            this.tbpCuadrado.Controls.Add(this.lblRespuestaCuadrado);
            this.tbpCuadrado.Controls.Add(this.btnCalcularCuadrado);
            this.tbpCuadrado.Controls.Add(this.txtLado);
            this.tbpCuadrado.Controls.Add(this.lblLado);
            this.tbpCuadrado.Location = new System.Drawing.Point(4, 24);
            this.tbpCuadrado.Name = "tbpCuadrado";
            this.tbpCuadrado.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCuadrado.Size = new System.Drawing.Size(584, 381);
            this.tbpCuadrado.TabIndex = 0;
            this.tbpCuadrado.Text = "Cuadrado";
            this.tbpCuadrado.UseVisualStyleBackColor = true;
            // 
            // lblRespuestaCuadrado
            // 
            this.lblRespuestaCuadrado.AutoSize = true;
            this.lblRespuestaCuadrado.Location = new System.Drawing.Point(43, 154);
            this.lblRespuestaCuadrado.Name = "lblRespuestaCuadrado";
            this.lblRespuestaCuadrado.Size = new System.Drawing.Size(57, 15);
            this.lblRespuestaCuadrado.TabIndex = 4;
            this.lblRespuestaCuadrado.Text = "respuesta";
            // 
            // btnCalcularCuadrado
            // 
            this.btnCalcularCuadrado.Location = new System.Drawing.Point(360, 57);
            this.btnCalcularCuadrado.Name = "btnCalcularCuadrado";
            this.btnCalcularCuadrado.Size = new System.Drawing.Size(75, 23);
            this.btnCalcularCuadrado.TabIndex = 3;
            this.btnCalcularCuadrado.Text = "Calcular";
            this.btnCalcularCuadrado.UseVisualStyleBackColor = true;
            this.btnCalcularCuadrado.Click += new System.EventHandler(this.btnCalcularCuadrado_Click);
            // 
            // txtLado
            // 
            this.txtLado.Location = new System.Drawing.Point(85, 57);
            this.txtLado.Name = "txtLado";
            this.txtLado.Size = new System.Drawing.Size(150, 23);
            this.txtLado.TabIndex = 2;
            // 
            // lblLado
            // 
            this.lblLado.AutoSize = true;
            this.lblLado.Location = new System.Drawing.Point(43, 60);
            this.lblLado.Name = "lblLado";
            this.lblLado.Size = new System.Drawing.Size(36, 15);
            this.lblLado.TabIndex = 1;
            this.lblLado.Text = "Lado:";
            // 
            // tbpCirculo
            // 
            this.tbpCirculo.Controls.Add(this.btnCalcularCirculo);
            this.tbpCirculo.Controls.Add(this.txtRadio);
            this.tbpCirculo.Controls.Add(this.lblRespuestaCirculo);
            this.tbpCirculo.Controls.Add(this.lblRadio);
            this.tbpCirculo.Location = new System.Drawing.Point(4, 24);
            this.tbpCirculo.Name = "tbpCirculo";
            this.tbpCirculo.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCirculo.Size = new System.Drawing.Size(584, 381);
            this.tbpCirculo.TabIndex = 1;
            this.tbpCirculo.Text = "Circulo";
            this.tbpCirculo.UseVisualStyleBackColor = true;
            // 
            // btnCalcularCirculo
            // 
            this.btnCalcularCirculo.Location = new System.Drawing.Point(294, 59);
            this.btnCalcularCirculo.Name = "btnCalcularCirculo";
            this.btnCalcularCirculo.Size = new System.Drawing.Size(75, 23);
            this.btnCalcularCirculo.TabIndex = 3;
            this.btnCalcularCirculo.Text = "Calcular";
            this.btnCalcularCirculo.UseVisualStyleBackColor = true;
            this.btnCalcularCirculo.Click += new System.EventHandler(this.btnCalcularCirculo_Click);
            // 
            // txtRadio
            // 
            this.txtRadio.Location = new System.Drawing.Point(110, 60);
            this.txtRadio.Name = "txtRadio";
            this.txtRadio.Size = new System.Drawing.Size(115, 23);
            this.txtRadio.TabIndex = 2;
            // 
            // lblRespuestaCirculo
            // 
            this.lblRespuestaCirculo.AutoSize = true;
            this.lblRespuestaCirculo.Location = new System.Drawing.Point(55, 159);
            this.lblRespuestaCirculo.Name = "lblRespuestaCirculo";
            this.lblRespuestaCirculo.Size = new System.Drawing.Size(60, 15);
            this.lblRespuestaCirculo.TabIndex = 1;
            this.lblRespuestaCirculo.Text = "Respuesta";
            // 
            // lblRadio
            // 
            this.lblRadio.AutoSize = true;
            this.lblRadio.Location = new System.Drawing.Point(55, 63);
            this.lblRadio.Name = "lblRadio";
            this.lblRadio.Size = new System.Drawing.Size(40, 15);
            this.lblRadio.TabIndex = 0;
            this.lblRadio.Text = "Radio:";
            // 
            // tboRombo
            // 
            this.tboRombo.Controls.Add(this.txtDiagonalMenor);
            this.tboRombo.Controls.Add(this.lblDiagonalMenor);
            this.tboRombo.Controls.Add(this.btnCalcularRombo);
            this.tboRombo.Controls.Add(this.txtDiagonalMayor);
            this.tboRombo.Controls.Add(this.lblRespuestaRombo);
            this.tboRombo.Controls.Add(this.lblDiagonalMayor);
            this.tboRombo.Location = new System.Drawing.Point(4, 24);
            this.tboRombo.Name = "tboRombo";
            this.tboRombo.Size = new System.Drawing.Size(584, 381);
            this.tboRombo.TabIndex = 2;
            this.tboRombo.Text = "Rombo";
            this.tboRombo.UseVisualStyleBackColor = true;
            // 
            // btnCalcularRombo
            // 
            this.btnCalcularRombo.Location = new System.Drawing.Point(351, 54);
            this.btnCalcularRombo.Name = "btnCalcularRombo";
            this.btnCalcularRombo.Size = new System.Drawing.Size(75, 23);
            this.btnCalcularRombo.TabIndex = 7;
            this.btnCalcularRombo.Text = "Calcular";
            this.btnCalcularRombo.UseVisualStyleBackColor = true;
            this.btnCalcularRombo.Click += new System.EventHandler(this.btnCalcularRombo_Click);
            // 
            // txtDiagonalMayor
            // 
            this.txtDiagonalMayor.Location = new System.Drawing.Point(124, 54);
            this.txtDiagonalMayor.Name = "txtDiagonalMayor";
            this.txtDiagonalMayor.Size = new System.Drawing.Size(123, 23);
            this.txtDiagonalMayor.TabIndex = 6;
            // 
            // lblRespuestaRombo
            // 
            this.lblRespuestaRombo.AutoSize = true;
            this.lblRespuestaRombo.Location = new System.Drawing.Point(24, 185);
            this.lblRespuestaRombo.Name = "lblRespuestaRombo";
            this.lblRespuestaRombo.Size = new System.Drawing.Size(60, 15);
            this.lblRespuestaRombo.TabIndex = 5;
            this.lblRespuestaRombo.Text = "Respuesta";
            // 
            // lblDiagonalMayor
            // 
            this.lblDiagonalMayor.AutoSize = true;
            this.lblDiagonalMayor.Location = new System.Drawing.Point(24, 58);
            this.lblDiagonalMayor.Name = "lblDiagonalMayor";
            this.lblDiagonalMayor.Size = new System.Drawing.Size(94, 15);
            this.lblDiagonalMayor.TabIndex = 4;
            this.lblDiagonalMayor.Text = "Diagonal mayor:";
            // 
            // lblDiagonalMenor
            // 
            this.lblDiagonalMenor.AutoSize = true;
            this.lblDiagonalMenor.Location = new System.Drawing.Point(24, 113);
            this.lblDiagonalMenor.Name = "lblDiagonalMenor";
            this.lblDiagonalMenor.Size = new System.Drawing.Size(95, 15);
            this.lblDiagonalMenor.TabIndex = 8;
            this.lblDiagonalMenor.Text = "Diagonal menor:";
            // 
            // txtDiagonalMenor
            // 
            this.txtDiagonalMenor.Location = new System.Drawing.Point(125, 110);
            this.txtDiagonalMenor.Name = "txtDiagonalMenor";
            this.txtDiagonalMenor.Size = new System.Drawing.Size(123, 23);
            this.txtDiagonalMenor.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 415);
            this.Controls.Add(this.tbContenedor);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tbContenedor.ResumeLayout(false);
            this.tbpCuadrado.ResumeLayout(false);
            this.tbpCuadrado.PerformLayout();
            this.tbpCirculo.ResumeLayout(false);
            this.tbpCirculo.PerformLayout();
            this.tboRombo.ResumeLayout(false);
            this.tboRombo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbContenedor;
        private System.Windows.Forms.TabPage tbpCuadrado;
        private System.Windows.Forms.TabPage tbpCirculo;
        private System.Windows.Forms.Label lblLado;
        private System.Windows.Forms.Button btnCalcularCuadrado;
        private System.Windows.Forms.TextBox txtLado;
        private System.Windows.Forms.Label lblRespuestaCuadrado;
        private System.Windows.Forms.Button btnCalcularCirculo;
        private System.Windows.Forms.TextBox txtRadio;
        private System.Windows.Forms.Label lblRespuestaCirculo;
        private System.Windows.Forms.Label lblRadio;
        private System.Windows.Forms.TabPage tboRombo;
        private System.Windows.Forms.TextBox txtDiagonalMenor;
        private System.Windows.Forms.Label lblDiagonalMenor;
        private System.Windows.Forms.Button btnCalcularRombo;
        private System.Windows.Forms.TextBox txtDiagonalMayor;
        private System.Windows.Forms.Label lblRespuestaRombo;
        private System.Windows.Forms.Label lblDiagonalMayor;
    }
}

