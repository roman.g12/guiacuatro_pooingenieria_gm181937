﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace guia4_complementario3
{
    public class Circulo : Figura
    {
        private double radio;

        public Circulo(double A, double R) : base(A) //Constructor con parámetro
        {
            Radio = R;
        }

        //Método sobreescrito
        public override void CalcularArea(Label LR)
        {
            Area = (Math.PI * Math.Pow(Radio, 2));
            LR.Text = "Area: " + Area;
        }

        public double Radio { get => radio; set => radio = value; }
    }
}
