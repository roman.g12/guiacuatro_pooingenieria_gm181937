﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace guia4_complementario3
{
    public abstract class Figura //Clase pública que no permite instancias
    {
        private double area;

        protected Figura(double A)
        {
            Area = A;
        }

        //Método utilizando virtual
        public virtual void CalcularArea(Label LR)
        {

        }

        public double Area { get => area; set => area = value; }
    }
}
