﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guia4_complementario3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcularCuadrado_Click(object sender, EventArgs e)
        {
            double L = double.Parse(txtLado.Text);
            double A = 0;

            Cuadrado cuadrado = new Cuadrado(A, L);
            cuadrado.CalcularArea(lblRespuestaCuadrado);
        }

        private void btnCalcularCirculo_Click(object sender, EventArgs e)
        {
            double R = double.Parse(txtRadio.Text);
            double A = 0;

            Circulo circulo = new Circulo(A, R);
            circulo.CalcularArea(lblRespuestaCirculo);
        }

        private void btnCalcularRombo_Click(object sender, EventArgs e)
        {
            double mayor = double.Parse(txtDiagonalMayor.Text);
            double menor = double.Parse(txtDiagonalMenor.Text);
            double A = 0;

            Rombo rombo = new Rombo(A, mayor, menor);
            rombo.CalcularArea(lblRespuestaRombo);
        }
    }
}
