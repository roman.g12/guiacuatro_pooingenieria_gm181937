﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia4_complementario2
{
    class Persona //Clase base
    {
        private List<Empleado> Empleado = new List<Empleado>();
        private List<Ingenieria> Estudiante = new List<Ingenieria>();

        string dui;
        string nombre;
        DateTime fechanac;
        string direccasa;
        int edadpersona;

        public Persona()
        {

        }

        public int Edad(DateTime nacimiento)
        {
            //DateTime nacimiento = new DateTime(2000, 1, 25); //Fecha de nacimiento
            int edad = DateTime.Today.AddTicks(-nacimiento.Ticks).Year - 1;

            return edad;
        }

        public List<Empleado> registrarEmpleado(string dui, string nombre, string direccion, string empresa, double salario, DateTime fechaNacimiento)
        {
            Empleado empleado = new Empleado();
            int edad;

            //Agregamos los datos de la clase
            empleado.Nombre = nombre;
            empleado.Dui = dui;
            empleado.Direccasa = direccion;
            empleado.Nombreempresa = empresa;
            empleado.Salario = salario;
            empleado.Fechanac = fechaNacimiento;

            //Calculamos la edad
            edad = Edad(fechaNacimiento);
            empleado.Edadpersona = edad;

            Empleado.Add(empleado); //Registramos en nuestra lista al empleado

            return Empleado; //Retornamos el list para actualizar el DataGrid
        }

        public List<Ingenieria> registrarEstudiante(string dui, string nombre, string direccion, string carnet, string nivelEstudio, string nombreU, string carrera, double cum, int materiasInscritas, double[] notas, string nombreProyecto, int totalHoras, int horasCompletadas, DateTime fechaNacimiento, double promedio, int contador)
        {
            Ingenieria estudiante = new Ingenieria();
            int edad;

            //Agregamos los datos de la clase
            estudiante.Dui = dui;
            estudiante.Nombre = nombre;
            estudiante.Direccasa = direccion;
            estudiante.Carnet = carnet;
            estudiante.Nivelestudio = nivelEstudio;
            estudiante.Nombreuniversidad = nombreU;
            estudiante.Carrera = carrera;
            estudiante.CUM = cum;
            estudiante.Materiasinscritas = materiasInscritas;

            for (int i = 0; i < contador; i++)
            {
                estudiante.Notas[i] = notas[i];
            }

            estudiante.Nombreproyecto = nombreProyecto;
            estudiante.Totalhoras = totalHoras;
            estudiante.Horascompletadas = horasCompletadas;
            estudiante.Promedio = promedio;

            //Calculamos la edad
            edad = Edad(fechaNacimiento);
            estudiante.Edadpersona = edad;

            Estudiante.Add(estudiante);

            return Estudiante;
        }

        public string Dui { get => dui; set => dui = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public DateTime Fechanac { get => fechanac; set => fechanac = value; }
        public string Direccasa { get => direccasa; set => direccasa = value; }
        public int Edadpersona { get => edadpersona; set => edadpersona = value; }
    }
}
