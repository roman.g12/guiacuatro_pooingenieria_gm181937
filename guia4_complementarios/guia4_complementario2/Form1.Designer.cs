﻿
namespace guia4_complementario2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbcRegistros = new System.Windows.Forms.TabControl();
            this.tbpEmpleados = new System.Windows.Forms.TabPage();
            this.btnRegistrarEmpleado = new System.Windows.Forms.Button();
            this.txtSalario = new System.Windows.Forms.TextBox();
            this.lblSalario = new System.Windows.Forms.Label();
            this.txtNombreEmpresa = new System.Windows.Forms.TextBox();
            this.lblNombreEmpresa = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNacimiento = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtDui = new System.Windows.Forms.TextBox();
            this.lblDUI = new System.Windows.Forms.Label();
            this.dgvDatosEmpleados = new System.Windows.Forms.DataGridView();
            this.tbpEstudiantes = new System.Windows.Forms.TabPage();
            this.grbRegistroIngenieria = new System.Windows.Forms.GroupBox();
            this.txtHorasCompletadas = new System.Windows.Forms.TextBox();
            this.lblHorasCompletas = new System.Windows.Forms.Label();
            this.txtTotalHoras = new System.Windows.Forms.TextBox();
            this.lblTotalHoras = new System.Windows.Forms.Label();
            this.txtNombreProyecto = new System.Windows.Forms.TextBox();
            this.lblNombreProyecto = new System.Windows.Forms.Label();
            this.btnRegistrarEstudiante = new System.Windows.Forms.Button();
            this.grbRegistroUniversitario = new System.Windows.Forms.GroupBox();
            this.btnRegistrarNota = new System.Windows.Forms.Button();
            this.txtCUM = new System.Windows.Forms.TextBox();
            this.lblCUM = new System.Windows.Forms.Label();
            this.txtNotas = new System.Windows.Forms.TextBox();
            this.lblNotas = new System.Windows.Forms.Label();
            this.cmbMateriasInscritas = new System.Windows.Forms.ComboBox();
            this.lblMateriasInscritas = new System.Windows.Forms.Label();
            this.txtCarrera = new System.Windows.Forms.TextBox();
            this.lblCarrera = new System.Windows.Forms.Label();
            this.txtNombreUniversidad = new System.Windows.Forms.TextBox();
            this.lblNombreUniversidad = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbNivelEstudio = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDireccionEstudiante = new System.Windows.Forms.TextBox();
            this.lblDireccionEstudiante = new System.Windows.Forms.Label();
            this.dtpFechaNacimientoEstudiante = new System.Windows.Forms.DateTimePicker();
            this.lblFechaNacimientoEstudiante = new System.Windows.Forms.Label();
            this.txtNombreEstudiante = new System.Windows.Forms.TextBox();
            this.lblNombreEstudiante = new System.Windows.Forms.Label();
            this.txtDuiEstudiante = new System.Windows.Forms.TextBox();
            this.lblDuiEstudiante = new System.Windows.Forms.Label();
            this.lblCarnet = new System.Windows.Forms.Label();
            this.txtCarnet = new System.Windows.Forms.TextBox();
            this.tbpInformacion = new System.Windows.Forms.TabPage();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnTodos = new System.Windows.Forms.Button();
            this.btnPromedio = new System.Windows.Forms.Button();
            this.btnRegistroPasantias = new System.Windows.Forms.Button();
            this.btnRegistroUniversitario = new System.Windows.Forms.Button();
            this.lblVerPor = new System.Windows.Forms.Label();
            this.btnDatosEstudiante = new System.Windows.Forms.Button();
            this.dgvDatosEstudiante = new System.Windows.Forms.DataGridView();
            this.tbcRegistros.SuspendLayout();
            this.tbpEmpleados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEmpleados)).BeginInit();
            this.tbpEstudiantes.SuspendLayout();
            this.grbRegistroIngenieria.SuspendLayout();
            this.grbRegistroUniversitario.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tbpInformacion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEstudiante)).BeginInit();
            this.SuspendLayout();
            // 
            // tbcRegistros
            // 
            this.tbcRegistros.Controls.Add(this.tbpEmpleados);
            this.tbcRegistros.Controls.Add(this.tbpEstudiantes);
            this.tbcRegistros.Controls.Add(this.tbpInformacion);
            this.tbcRegistros.Location = new System.Drawing.Point(12, 12);
            this.tbcRegistros.Name = "tbcRegistros";
            this.tbcRegistros.SelectedIndex = 0;
            this.tbcRegistros.Size = new System.Drawing.Size(801, 481);
            this.tbcRegistros.TabIndex = 1;
            // 
            // tbpEmpleados
            // 
            this.tbpEmpleados.Controls.Add(this.btnRegistrarEmpleado);
            this.tbpEmpleados.Controls.Add(this.txtSalario);
            this.tbpEmpleados.Controls.Add(this.lblSalario);
            this.tbpEmpleados.Controls.Add(this.txtNombreEmpresa);
            this.tbpEmpleados.Controls.Add(this.lblNombreEmpresa);
            this.tbpEmpleados.Controls.Add(this.txtDireccion);
            this.tbpEmpleados.Controls.Add(this.lblDireccion);
            this.tbpEmpleados.Controls.Add(this.dtpFechaNacimiento);
            this.tbpEmpleados.Controls.Add(this.lblFechaNacimiento);
            this.tbpEmpleados.Controls.Add(this.txtNombre);
            this.tbpEmpleados.Controls.Add(this.lblNombre);
            this.tbpEmpleados.Controls.Add(this.txtDui);
            this.tbpEmpleados.Controls.Add(this.lblDUI);
            this.tbpEmpleados.Controls.Add(this.dgvDatosEmpleados);
            this.tbpEmpleados.Location = new System.Drawing.Point(4, 24);
            this.tbpEmpleados.Name = "tbpEmpleados";
            this.tbpEmpleados.Padding = new System.Windows.Forms.Padding(3);
            this.tbpEmpleados.Size = new System.Drawing.Size(793, 453);
            this.tbpEmpleados.TabIndex = 0;
            this.tbpEmpleados.Text = "Registrar empleados";
            this.tbpEmpleados.UseVisualStyleBackColor = true;
            // 
            // btnRegistrarEmpleado
            // 
            this.btnRegistrarEmpleado.Location = new System.Drawing.Point(671, 173);
            this.btnRegistrarEmpleado.Name = "btnRegistrarEmpleado";
            this.btnRegistrarEmpleado.Size = new System.Drawing.Size(101, 42);
            this.btnRegistrarEmpleado.TabIndex = 13;
            this.btnRegistrarEmpleado.Text = "Registrar Empleado";
            this.btnRegistrarEmpleado.UseVisualStyleBackColor = true;
            this.btnRegistrarEmpleado.Click += new System.EventHandler(this.btnRegistrarEmpleado_Click);
            // 
            // txtSalario
            // 
            this.txtSalario.Location = new System.Drawing.Point(561, 76);
            this.txtSalario.Name = "txtSalario";
            this.txtSalario.Size = new System.Drawing.Size(100, 23);
            this.txtSalario.TabIndex = 12;
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.Location = new System.Drawing.Point(416, 82);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblSalario.Size = new System.Drawing.Size(45, 15);
            this.lblSalario.TabIndex = 11;
            this.lblSalario.Text = "Salario:";
            // 
            // txtNombreEmpresa
            // 
            this.txtNombreEmpresa.Location = new System.Drawing.Point(561, 22);
            this.txtNombreEmpresa.Name = "txtNombreEmpresa";
            this.txtNombreEmpresa.Size = new System.Drawing.Size(200, 23);
            this.txtNombreEmpresa.TabIndex = 10;
            // 
            // lblNombreEmpresa
            // 
            this.lblNombreEmpresa.AutoSize = true;
            this.lblNombreEmpresa.Location = new System.Drawing.Point(416, 28);
            this.lblNombreEmpresa.Name = "lblNombreEmpresa";
            this.lblNombreEmpresa.Size = new System.Drawing.Size(130, 15);
            this.lblNombreEmpresa.TabIndex = 9;
            this.lblNombreEmpresa.Text = "Nombre de la empresa:";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Location = new System.Drawing.Point(145, 192);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(200, 23);
            this.txtDireccion.TabIndex = 8;
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Location = new System.Drawing.Point(21, 195);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(60, 15);
            this.lblDireccion.TabIndex = 7;
            this.lblDireccion.Text = "Dirección:";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(145, 130);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(136, 23);
            this.dtpFechaNacimiento.TabIndex = 6;
            // 
            // lblFechaNacimiento
            // 
            this.lblFechaNacimiento.AutoSize = true;
            this.lblFechaNacimiento.Location = new System.Drawing.Point(21, 139);
            this.lblFechaNacimiento.Name = "lblFechaNacimiento";
            this.lblFechaNacimiento.Size = new System.Drawing.Size(120, 15);
            this.lblFechaNacimiento.TabIndex = 5;
            this.lblFechaNacimiento.Text = "Fecha de nacimineto:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(145, 76);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(200, 23);
            this.txtNombre.TabIndex = 4;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(21, 82);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(54, 15);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "Nombre:";
            // 
            // txtDui
            // 
            this.txtDui.Location = new System.Drawing.Point(145, 22);
            this.txtDui.Name = "txtDui";
            this.txtDui.Size = new System.Drawing.Size(200, 23);
            this.txtDui.TabIndex = 2;
            // 
            // lblDUI
            // 
            this.lblDUI.AutoSize = true;
            this.lblDUI.Location = new System.Drawing.Point(21, 28);
            this.lblDUI.Name = "lblDUI";
            this.lblDUI.Size = new System.Drawing.Size(29, 15);
            this.lblDUI.TabIndex = 1;
            this.lblDUI.Text = "DUI:";
            // 
            // dgvDatosEmpleados
            // 
            this.dgvDatosEmpleados.AllowUserToAddRows = false;
            this.dgvDatosEmpleados.AllowUserToDeleteRows = false;
            this.dgvDatosEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosEmpleados.Location = new System.Drawing.Point(125, 239);
            this.dgvDatosEmpleados.Name = "dgvDatosEmpleados";
            this.dgvDatosEmpleados.ReadOnly = true;
            this.dgvDatosEmpleados.RowTemplate.Height = 25;
            this.dgvDatosEmpleados.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDatosEmpleados.Size = new System.Drawing.Size(555, 210);
            this.dgvDatosEmpleados.TabIndex = 0;
            // 
            // tbpEstudiantes
            // 
            this.tbpEstudiantes.Controls.Add(this.grbRegistroIngenieria);
            this.tbpEstudiantes.Controls.Add(this.grbRegistroUniversitario);
            this.tbpEstudiantes.Controls.Add(this.groupBox1);
            this.tbpEstudiantes.Location = new System.Drawing.Point(4, 24);
            this.tbpEstudiantes.Name = "tbpEstudiantes";
            this.tbpEstudiantes.Padding = new System.Windows.Forms.Padding(3);
            this.tbpEstudiantes.Size = new System.Drawing.Size(793, 453);
            this.tbpEstudiantes.TabIndex = 1;
            this.tbpEstudiantes.Text = "Resgistrar estudiantes";
            this.tbpEstudiantes.UseVisualStyleBackColor = true;
            // 
            // grbRegistroIngenieria
            // 
            this.grbRegistroIngenieria.Controls.Add(this.txtHorasCompletadas);
            this.grbRegistroIngenieria.Controls.Add(this.lblHorasCompletas);
            this.grbRegistroIngenieria.Controls.Add(this.txtTotalHoras);
            this.grbRegistroIngenieria.Controls.Add(this.lblTotalHoras);
            this.grbRegistroIngenieria.Controls.Add(this.txtNombreProyecto);
            this.grbRegistroIngenieria.Controls.Add(this.lblNombreProyecto);
            this.grbRegistroIngenieria.Controls.Add(this.btnRegistrarEstudiante);
            this.grbRegistroIngenieria.Location = new System.Drawing.Point(6, 248);
            this.grbRegistroIngenieria.Name = "grbRegistroIngenieria";
            this.grbRegistroIngenieria.Size = new System.Drawing.Size(769, 199);
            this.grbRegistroIngenieria.TabIndex = 30;
            this.grbRegistroIngenieria.TabStop = false;
            this.grbRegistroIngenieria.Text = "Registro ingeniería";
            // 
            // txtHorasCompletadas
            // 
            this.txtHorasCompletadas.Location = new System.Drawing.Point(167, 128);
            this.txtHorasCompletadas.Name = "txtHorasCompletadas";
            this.txtHorasCompletadas.Size = new System.Drawing.Size(79, 23);
            this.txtHorasCompletadas.TabIndex = 46;
            // 
            // lblHorasCompletas
            // 
            this.lblHorasCompletas.AutoSize = true;
            this.lblHorasCompletas.Location = new System.Drawing.Point(17, 131);
            this.lblHorasCompletas.Name = "lblHorasCompletas";
            this.lblHorasCompletas.Size = new System.Drawing.Size(112, 15);
            this.lblHorasCompletas.TabIndex = 45;
            this.lblHorasCompletas.Text = "Horas completadas:";
            // 
            // txtTotalHoras
            // 
            this.txtTotalHoras.Location = new System.Drawing.Point(167, 83);
            this.txtTotalHoras.Name = "txtTotalHoras";
            this.txtTotalHoras.Size = new System.Drawing.Size(79, 23);
            this.txtTotalHoras.TabIndex = 44;
            // 
            // lblTotalHoras
            // 
            this.lblTotalHoras.AutoSize = true;
            this.lblTotalHoras.Location = new System.Drawing.Point(17, 86);
            this.lblTotalHoras.Name = "lblTotalHoras";
            this.lblTotalHoras.Size = new System.Drawing.Size(130, 15);
            this.lblTotalHoras.TabIndex = 43;
            this.lblTotalHoras.Text = "Total horas de pasantía:";
            // 
            // txtNombreProyecto
            // 
            this.txtNombreProyecto.Location = new System.Drawing.Point(167, 43);
            this.txtNombreProyecto.Name = "txtNombreProyecto";
            this.txtNombreProyecto.Size = new System.Drawing.Size(254, 23);
            this.txtNombreProyecto.TabIndex = 27;
            // 
            // lblNombreProyecto
            // 
            this.lblNombreProyecto.AutoSize = true;
            this.lblNombreProyecto.Location = new System.Drawing.Point(17, 46);
            this.lblNombreProyecto.Name = "lblNombreProyecto";
            this.lblNombreProyecto.Size = new System.Drawing.Size(123, 15);
            this.lblNombreProyecto.TabIndex = 42;
            this.lblNombreProyecto.Text = "Nombre del proyecto:";
            // 
            // btnRegistrarEstudiante
            // 
            this.btnRegistrarEstudiante.Enabled = false;
            this.btnRegistrarEstudiante.Location = new System.Drawing.Point(662, 151);
            this.btnRegistrarEstudiante.Name = "btnRegistrarEstudiante";
            this.btnRegistrarEstudiante.Size = new System.Drawing.Size(101, 42);
            this.btnRegistrarEstudiante.TabIndex = 22;
            this.btnRegistrarEstudiante.Text = "Registrar Estudiante";
            this.btnRegistrarEstudiante.UseVisualStyleBackColor = true;
            this.btnRegistrarEstudiante.Click += new System.EventHandler(this.btnRegistrarEstudiante_Click);
            // 
            // grbRegistroUniversitario
            // 
            this.grbRegistroUniversitario.Controls.Add(this.btnRegistrarNota);
            this.grbRegistroUniversitario.Controls.Add(this.txtCUM);
            this.grbRegistroUniversitario.Controls.Add(this.lblCUM);
            this.grbRegistroUniversitario.Controls.Add(this.txtNotas);
            this.grbRegistroUniversitario.Controls.Add(this.lblNotas);
            this.grbRegistroUniversitario.Controls.Add(this.cmbMateriasInscritas);
            this.grbRegistroUniversitario.Controls.Add(this.lblMateriasInscritas);
            this.grbRegistroUniversitario.Controls.Add(this.txtCarrera);
            this.grbRegistroUniversitario.Controls.Add(this.lblCarrera);
            this.grbRegistroUniversitario.Controls.Add(this.txtNombreUniversidad);
            this.grbRegistroUniversitario.Controls.Add(this.lblNombreUniversidad);
            this.grbRegistroUniversitario.Location = new System.Drawing.Point(389, 9);
            this.grbRegistroUniversitario.Name = "grbRegistroUniversitario";
            this.grbRegistroUniversitario.Size = new System.Drawing.Size(386, 233);
            this.grbRegistroUniversitario.TabIndex = 29;
            this.grbRegistroUniversitario.TabStop = false;
            this.grbRegistroUniversitario.Text = "Registro Universitario";
            // 
            // btnRegistrarNota
            // 
            this.btnRegistrarNota.Enabled = false;
            this.btnRegistrarNota.Location = new System.Drawing.Point(261, 193);
            this.btnRegistrarNota.Name = "btnRegistrarNota";
            this.btnRegistrarNota.Size = new System.Drawing.Size(100, 23);
            this.btnRegistrarNota.TabIndex = 41;
            this.btnRegistrarNota.Text = "Registrar nota";
            this.btnRegistrarNota.UseVisualStyleBackColor = true;
            this.btnRegistrarNota.Click += new System.EventHandler(this.btnRegistrarNota_Click);
            // 
            // txtCUM
            // 
            this.txtCUM.Location = new System.Drawing.Point(136, 103);
            this.txtCUM.Name = "txtCUM";
            this.txtCUM.Size = new System.Drawing.Size(160, 23);
            this.txtCUM.TabIndex = 40;
            // 
            // lblCUM
            // 
            this.lblCUM.AutoSize = true;
            this.lblCUM.Location = new System.Drawing.Point(6, 106);
            this.lblCUM.Name = "lblCUM";
            this.lblCUM.Size = new System.Drawing.Size(37, 15);
            this.lblCUM.TabIndex = 39;
            this.lblCUM.Text = "CUM:";
            // 
            // txtNotas
            // 
            this.txtNotas.Enabled = false;
            this.txtNotas.Location = new System.Drawing.Point(261, 145);
            this.txtNotas.Name = "txtNotas";
            this.txtNotas.Size = new System.Drawing.Size(100, 23);
            this.txtNotas.TabIndex = 38;
            // 
            // lblNotas
            // 
            this.lblNotas.AutoSize = true;
            this.lblNotas.Location = new System.Drawing.Point(214, 149);
            this.lblNotas.Name = "lblNotas";
            this.lblNotas.Size = new System.Drawing.Size(41, 15);
            this.lblNotas.TabIndex = 37;
            this.lblNotas.Text = "Notas:";
            // 
            // cmbMateriasInscritas
            // 
            this.cmbMateriasInscritas.FormattingEnabled = true;
            this.cmbMateriasInscritas.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbMateriasInscritas.Location = new System.Drawing.Point(136, 146);
            this.cmbMateriasInscritas.Name = "cmbMateriasInscritas";
            this.cmbMateriasInscritas.Size = new System.Drawing.Size(57, 23);
            this.cmbMateriasInscritas.TabIndex = 36;
            this.cmbMateriasInscritas.SelectedIndexChanged += new System.EventHandler(this.cmbMateriasInscritas_SelectedIndexChanged);
            // 
            // lblMateriasInscritas
            // 
            this.lblMateriasInscritas.AutoSize = true;
            this.lblMateriasInscritas.Location = new System.Drawing.Point(6, 149);
            this.lblMateriasInscritas.Name = "lblMateriasInscritas";
            this.lblMateriasInscritas.Size = new System.Drawing.Size(101, 15);
            this.lblMateriasInscritas.TabIndex = 35;
            this.lblMateriasInscritas.Text = "Materias inscritas:";
            // 
            // txtCarrera
            // 
            this.txtCarrera.Location = new System.Drawing.Point(136, 64);
            this.txtCarrera.Name = "txtCarrera";
            this.txtCarrera.Size = new System.Drawing.Size(160, 23);
            this.txtCarrera.TabIndex = 34;
            // 
            // lblCarrera
            // 
            this.lblCarrera.AutoSize = true;
            this.lblCarrera.Location = new System.Drawing.Point(6, 67);
            this.lblCarrera.Name = "lblCarrera";
            this.lblCarrera.Size = new System.Drawing.Size(48, 15);
            this.lblCarrera.TabIndex = 33;
            this.lblCarrera.Text = "Carrera:";
            // 
            // txtNombreUniversidad
            // 
            this.txtNombreUniversidad.Location = new System.Drawing.Point(136, 22);
            this.txtNombreUniversidad.Name = "txtNombreUniversidad";
            this.txtNombreUniversidad.Size = new System.Drawing.Size(160, 23);
            this.txtNombreUniversidad.TabIndex = 28;
            // 
            // lblNombreUniversidad
            // 
            this.lblNombreUniversidad.AutoSize = true;
            this.lblNombreUniversidad.Location = new System.Drawing.Point(6, 28);
            this.lblNombreUniversidad.Name = "lblNombreUniversidad";
            this.lblNombreUniversidad.Size = new System.Drawing.Size(119, 15);
            this.lblNombreUniversidad.TabIndex = 27;
            this.lblNombreUniversidad.Text = "Nombre Universidad:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbNivelEstudio);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDireccionEstudiante);
            this.groupBox1.Controls.Add(this.lblDireccionEstudiante);
            this.groupBox1.Controls.Add(this.dtpFechaNacimientoEstudiante);
            this.groupBox1.Controls.Add(this.lblFechaNacimientoEstudiante);
            this.groupBox1.Controls.Add(this.txtNombreEstudiante);
            this.groupBox1.Controls.Add(this.lblNombreEstudiante);
            this.groupBox1.Controls.Add(this.txtDuiEstudiante);
            this.groupBox1.Controls.Add(this.lblDuiEstudiante);
            this.groupBox1.Controls.Add(this.lblCarnet);
            this.groupBox1.Controls.Add(this.txtCarnet);
            this.groupBox1.Location = new System.Drawing.Point(6, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(377, 233);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos del estudiante";
            // 
            // cmbNivelEstudio
            // 
            this.cmbNivelEstudio.FormattingEnabled = true;
            this.cmbNivelEstudio.Items.AddRange(new object[] {
            "Primarios",
            "Profesional",
            "Secundarios",
            "Medio-Superior",
            "Superior"});
            this.cmbNivelEstudio.Location = new System.Drawing.Point(252, 193);
            this.cmbNivelEstudio.Name = "cmbNivelEstudio";
            this.cmbNivelEstudio.Size = new System.Drawing.Size(119, 23);
            this.cmbNivelEstudio.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 15);
            this.label3.TabIndex = 25;
            this.label3.Text = "Nivel estudio:";
            // 
            // txtDireccionEstudiante
            // 
            this.txtDireccionEstudiante.Location = new System.Drawing.Point(141, 149);
            this.txtDireccionEstudiante.Name = "txtDireccionEstudiante";
            this.txtDireccionEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtDireccionEstudiante.TabIndex = 24;
            // 
            // lblDireccionEstudiante
            // 
            this.lblDireccionEstudiante.AutoSize = true;
            this.lblDireccionEstudiante.Location = new System.Drawing.Point(17, 152);
            this.lblDireccionEstudiante.Name = "lblDireccionEstudiante";
            this.lblDireccionEstudiante.Size = new System.Drawing.Size(60, 15);
            this.lblDireccionEstudiante.TabIndex = 23;
            this.lblDireccionEstudiante.Text = "Dirección:";
            // 
            // dtpFechaNacimientoEstudiante
            // 
            this.dtpFechaNacimientoEstudiante.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimientoEstudiante.Location = new System.Drawing.Point(143, 106);
            this.dtpFechaNacimientoEstudiante.Name = "dtpFechaNacimientoEstudiante";
            this.dtpFechaNacimientoEstudiante.Size = new System.Drawing.Size(136, 23);
            this.dtpFechaNacimientoEstudiante.TabIndex = 22;
            // 
            // lblFechaNacimientoEstudiante
            // 
            this.lblFechaNacimientoEstudiante.AutoSize = true;
            this.lblFechaNacimientoEstudiante.Location = new System.Drawing.Point(17, 112);
            this.lblFechaNacimientoEstudiante.Name = "lblFechaNacimientoEstudiante";
            this.lblFechaNacimientoEstudiante.Size = new System.Drawing.Size(120, 15);
            this.lblFechaNacimientoEstudiante.TabIndex = 21;
            this.lblFechaNacimientoEstudiante.Text = "Fecha de nacimineto:";
            // 
            // txtNombreEstudiante
            // 
            this.txtNombreEstudiante.Location = new System.Drawing.Point(141, 64);
            this.txtNombreEstudiante.Name = "txtNombreEstudiante";
            this.txtNombreEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtNombreEstudiante.TabIndex = 20;
            // 
            // lblNombreEstudiante
            // 
            this.lblNombreEstudiante.AutoSize = true;
            this.lblNombreEstudiante.Location = new System.Drawing.Point(17, 67);
            this.lblNombreEstudiante.Name = "lblNombreEstudiante";
            this.lblNombreEstudiante.Size = new System.Drawing.Size(54, 15);
            this.lblNombreEstudiante.TabIndex = 19;
            this.lblNombreEstudiante.Text = "Nombre:";
            // 
            // txtDuiEstudiante
            // 
            this.txtDuiEstudiante.Location = new System.Drawing.Point(141, 22);
            this.txtDuiEstudiante.Name = "txtDuiEstudiante";
            this.txtDuiEstudiante.Size = new System.Drawing.Size(200, 23);
            this.txtDuiEstudiante.TabIndex = 18;
            // 
            // lblDuiEstudiante
            // 
            this.lblDuiEstudiante.AutoSize = true;
            this.lblDuiEstudiante.Location = new System.Drawing.Point(17, 28);
            this.lblDuiEstudiante.Name = "lblDuiEstudiante";
            this.lblDuiEstudiante.Size = new System.Drawing.Size(29, 15);
            this.lblDuiEstudiante.TabIndex = 17;
            this.lblDuiEstudiante.Text = "DUI:";
            // 
            // lblCarnet
            // 
            this.lblCarnet.AutoSize = true;
            this.lblCarnet.Location = new System.Drawing.Point(17, 196);
            this.lblCarnet.Name = "lblCarnet";
            this.lblCarnet.Size = new System.Drawing.Size(45, 15);
            this.lblCarnet.TabIndex = 17;
            this.lblCarnet.Text = "Carnet:";
            // 
            // txtCarnet
            // 
            this.txtCarnet.Location = new System.Drawing.Point(68, 193);
            this.txtCarnet.Name = "txtCarnet";
            this.txtCarnet.Size = new System.Drawing.Size(93, 23);
            this.txtCarnet.TabIndex = 18;
            // 
            // tbpInformacion
            // 
            this.tbpInformacion.Controls.Add(this.btnLimpiar);
            this.tbpInformacion.Controls.Add(this.btnTodos);
            this.tbpInformacion.Controls.Add(this.btnPromedio);
            this.tbpInformacion.Controls.Add(this.btnRegistroPasantias);
            this.tbpInformacion.Controls.Add(this.btnRegistroUniversitario);
            this.tbpInformacion.Controls.Add(this.lblVerPor);
            this.tbpInformacion.Controls.Add(this.btnDatosEstudiante);
            this.tbpInformacion.Controls.Add(this.dgvDatosEstudiante);
            this.tbpInformacion.Location = new System.Drawing.Point(4, 24);
            this.tbpInformacion.Name = "tbpInformacion";
            this.tbpInformacion.Padding = new System.Windows.Forms.Padding(3);
            this.tbpInformacion.Size = new System.Drawing.Size(793, 453);
            this.tbpInformacion.TabIndex = 2;
            this.tbpInformacion.Text = "Información de estudiante";
            this.tbpInformacion.UseVisualStyleBackColor = true;
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.Location = new System.Drawing.Point(6, 12);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(75, 33);
            this.btnLimpiar.TabIndex = 9;
            this.btnLimpiar.Text = "Limpiar";
            this.btnLimpiar.UseVisualStyleBackColor = true;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnTodos
            // 
            this.btnTodos.Location = new System.Drawing.Point(317, 12);
            this.btnTodos.Name = "btnTodos";
            this.btnTodos.Size = new System.Drawing.Size(70, 23);
            this.btnTodos.TabIndex = 8;
            this.btnTodos.Text = "Todos";
            this.btnTodos.UseVisualStyleBackColor = true;
            this.btnTodos.Click += new System.EventHandler(this.btnTodos_Click);
            // 
            // btnPromedio
            // 
            this.btnPromedio.Location = new System.Drawing.Point(646, 37);
            this.btnPromedio.Name = "btnPromedio";
            this.btnPromedio.Size = new System.Drawing.Size(120, 23);
            this.btnPromedio.TabIndex = 7;
            this.btnPromedio.Text = "Promedio de notas";
            this.btnPromedio.UseVisualStyleBackColor = true;
            this.btnPromedio.Click += new System.EventHandler(this.btnPromedio_Click);
            // 
            // btnRegistroPasantias
            // 
            this.btnRegistroPasantias.Location = new System.Drawing.Point(527, 37);
            this.btnRegistroPasantias.Name = "btnRegistroPasantias";
            this.btnRegistroPasantias.Size = new System.Drawing.Size(113, 23);
            this.btnRegistroPasantias.TabIndex = 6;
            this.btnRegistroPasantias.Text = "Registro pasantías";
            this.btnRegistroPasantias.UseVisualStyleBackColor = true;
            this.btnRegistroPasantias.Click += new System.EventHandler(this.btnRegistroPasantias_Click);
            // 
            // btnRegistroUniversitario
            // 
            this.btnRegistroUniversitario.Location = new System.Drawing.Point(395, 36);
            this.btnRegistroUniversitario.Name = "btnRegistroUniversitario";
            this.btnRegistroUniversitario.Size = new System.Drawing.Size(126, 23);
            this.btnRegistroUniversitario.TabIndex = 5;
            this.btnRegistroUniversitario.Text = "Registro universitario";
            this.btnRegistroUniversitario.UseVisualStyleBackColor = true;
            this.btnRegistroUniversitario.Click += new System.EventHandler(this.btnRegistroUniversitario_Click);
            // 
            // lblVerPor
            // 
            this.lblVerPor.AutoSize = true;
            this.lblVerPor.Location = new System.Drawing.Point(285, 16);
            this.lblVerPor.Name = "lblVerPor";
            this.lblVerPor.Size = new System.Drawing.Size(26, 15);
            this.lblVerPor.TabIndex = 4;
            this.lblVerPor.Text = "Ver:";
            // 
            // btnDatosEstudiante
            // 
            this.btnDatosEstudiante.Location = new System.Drawing.Point(285, 37);
            this.btnDatosEstudiante.Name = "btnDatosEstudiante";
            this.btnDatosEstudiante.Size = new System.Drawing.Size(104, 23);
            this.btnDatosEstudiante.TabIndex = 3;
            this.btnDatosEstudiante.Text = "Datos estudiante";
            this.btnDatosEstudiante.UseVisualStyleBackColor = true;
            this.btnDatosEstudiante.Click += new System.EventHandler(this.btnDatosEstudiante_Click);
            // 
            // dgvDatosEstudiante
            // 
            this.dgvDatosEstudiante.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDatosEstudiante.Location = new System.Drawing.Point(6, 76);
            this.dgvDatosEstudiante.Name = "dgvDatosEstudiante";
            this.dgvDatosEstudiante.RowTemplate.Height = 25;
            this.dgvDatosEstudiante.Size = new System.Drawing.Size(781, 371);
            this.dgvDatosEstudiante.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 505);
            this.Controls.Add(this.tbcRegistros);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tbcRegistros.ResumeLayout(false);
            this.tbpEmpleados.ResumeLayout(false);
            this.tbpEmpleados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEmpleados)).EndInit();
            this.tbpEstudiantes.ResumeLayout(false);
            this.grbRegistroIngenieria.ResumeLayout(false);
            this.grbRegistroIngenieria.PerformLayout();
            this.grbRegistroUniversitario.ResumeLayout(false);
            this.grbRegistroUniversitario.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tbpInformacion.ResumeLayout(false);
            this.tbpInformacion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDatosEstudiante)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tbcRegistros;
        private System.Windows.Forms.TabPage tbpEmpleados;
        private System.Windows.Forms.Button btnRegistrarEmpleado;
        private System.Windows.Forms.TextBox txtSalario;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.TextBox txtNombreEmpresa;
        private System.Windows.Forms.Label lblNombreEmpresa;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label lblFechaNacimiento;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtDui;
        private System.Windows.Forms.Label lblDUI;
        private System.Windows.Forms.DataGridView dgvDatosEmpleados;
        private System.Windows.Forms.TabPage tbpEstudiantes;
        private System.Windows.Forms.Button btnRegistrarEstudiante;
        private System.Windows.Forms.TextBox txtCarnet;
        private System.Windows.Forms.Label lblCarnet;
        private System.Windows.Forms.TabPage tbpInformacion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtDireccionEstudiante;
        private System.Windows.Forms.Label lblDireccionEstudiante;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimientoEstudiante;
        private System.Windows.Forms.Label lblFechaNacimientoEstudiante;
        private System.Windows.Forms.TextBox txtNombreEstudiante;
        private System.Windows.Forms.Label lblNombreEstudiante;
        private System.Windows.Forms.TextBox txtDuiEstudiante;
        private System.Windows.Forms.Label lblDuiEstudiante;
        private System.Windows.Forms.ComboBox cmbNivelEstudio;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox grbRegistroUniversitario;
        private System.Windows.Forms.TextBox txtNombreUniversidad;
        private System.Windows.Forms.Label lblNombreUniversidad;
        private System.Windows.Forms.TextBox txtCarrera;
        private System.Windows.Forms.Label lblCarrera;
        private System.Windows.Forms.TextBox txtNotas;
        private System.Windows.Forms.Label lblNotas;
        private System.Windows.Forms.ComboBox cmbMateriasInscritas;
        private System.Windows.Forms.Label lblMateriasInscritas;
        private System.Windows.Forms.Button btnRegistrarNota;
        private System.Windows.Forms.TextBox txtCUM;
        private System.Windows.Forms.Label lblCUM;
        private System.Windows.Forms.GroupBox grbRegistroIngenieria;
        private System.Windows.Forms.TextBox txtHorasCompletadas;
        private System.Windows.Forms.Label lblHorasCompletas;
        private System.Windows.Forms.TextBox txtTotalHoras;
        private System.Windows.Forms.Label lblTotalHoras;
        private System.Windows.Forms.TextBox txtNombreProyecto;
        private System.Windows.Forms.Label lblNombreProyecto;
        private System.Windows.Forms.Button btnPromedio;
        private System.Windows.Forms.Button btnRegistroPasantias;
        private System.Windows.Forms.Button btnRegistroUniversitario;
        private System.Windows.Forms.Label lblVerPor;
        private System.Windows.Forms.Button btnDatosEstudiante;
        private System.Windows.Forms.DataGridView dgvDatosEstudiante;
        private System.Windows.Forms.Button btnTodos;
        private System.Windows.Forms.Button btnLimpiar;
    }
}

