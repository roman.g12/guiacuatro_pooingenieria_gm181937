﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia4_complementario2
{
    class Ingenieria : Universitario
    {
        string nombreproyecto;
        int totalhoras;
        int horascompletadas;

        public Ingenieria()
        {

        }

        public string Nombreproyecto { get => nombreproyecto; set => nombreproyecto = value; }
        public int Totalhoras { get => totalhoras; set => totalhoras = value; }
        public int Horascompletadas { get => horascompletadas; set => horascompletadas = value; }
    }
}
