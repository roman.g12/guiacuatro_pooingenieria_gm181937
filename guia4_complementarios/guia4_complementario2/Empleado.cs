﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia4_complementario2
{
    class Empleado : Persona
    {
        private string nombreempresa;
        private double salario;

        public Empleado()
        {
            
        }

        public string Nombreempresa { get => nombreempresa; set => nombreempresa = value; }
        public double Salario { get => salario; set => salario = value; }
    }
}
