﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace guia4_complementario2
{
    public partial class Form1 : Form
    {
        //Listas con la que llenaremos el dataGrid
        private List<Empleado> Empleado = new List<Empleado>();
        private List<Ingenieria> Estudiante = new List<Ingenieria>();
        //Instancias de las clases
        Empleado empleado = new Empleado();
        Ingenieria estudiante = new Ingenieria();

        //Arreglo que guardara las notas del estudiante
        double[] notas = new double[4];
        int contador = 0; //Contador que nos ayudara para el registro de las notas
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRegistrarEmpleado_Click(object sender, EventArgs e)
        {
            //Validamos que los campos no esten vacíos
            if (txtDui.Text != "" && txtNombre.Text != "" && txtDireccion.Text != "" && txtNombreEmpresa.Text != "" && txtSalario.Text != "")
            {
                //Validamos que la edad mímima para registrar a un empleado sean 17 años
                if (dtpFechaNacimiento.Value.Year <= 2004)
                {
                    //Invocar método que registrara a los Empleados y los guardara en una lista
                    Empleado = empleado.registrarEmpleado(txtDui.Text, txtNombre.Text, txtDireccion.Text, txtNombreEmpresa.Text, double.Parse(txtSalario.Text), dtpFechaNacimiento.Value);

                    dgvDatosEmpleados.DataSource = null;
                    dgvDatosEmpleados.DataSource = Empleado; /*los nombres de columna que veremos son los de las propiedades*/

                    //Orientamos las columas
                    dgvDatosEmpleados.Columns["Nombre"].DisplayIndex = 0;
                    dgvDatosEmpleados.Columns["Dui"].DisplayIndex = 1;
                    dgvDatosEmpleados.Columns["Salario"].DisplayIndex = 2;
                    dgvDatosEmpleados.Columns["Nombreempresa"].DisplayIndex = 3;
                    dgvDatosEmpleados.Columns["Fechanac"].DisplayIndex = 4;
                    dgvDatosEmpleados.Columns["Direccasa"].DisplayIndex = 5;
                    dgvDatosEmpleados.Columns["Edadpersona"].DisplayIndex = 6;
                    //Cambiamos los headers de algunas columnas
                    dgvDatosEmpleados.Columns["Nombreempresa"].HeaderText = "Nombre empresa";
                    dgvDatosEmpleados.Columns["Edadpersona"].HeaderText = "Edad";
                    //No mostramos los campos de fecha nacimiento y dirección
                    dgvDatosEmpleados.Columns["Fechanac"].Visible = false;
                    dgvDatosEmpleados.Columns["Direccasa"].Visible = false;

                    //limpiamos las txt
                    txtNombre.Clear();
                    txtDui.Clear();
                    txtDireccion.Clear();
                    txtSalario.Clear();
                    txtNombreEmpresa.Clear();
                }
                else
                {
                    MessageBox.Show("No es posible registrar empleados menores a 17 años");
                }
            }
            else
            {
                MessageBox.Show("No deje campos vacíos");
            }

            /*foreach (Empleado empl in Empleado)
            {
                MessageBox.Show(empl.Nombre);
            }*/
        }

        private void btnRegistrarEstudiante_Click(object sender, EventArgs e)
        {
            double sumaPromedio = 0;
            //Validamos que ningún campo quede vacío
            if (txtNombreEstudiante.Text != "" && txtDuiEstudiante.Text != "" && txtDireccionEstudiante.Text != "" && txtCarnet.Text != "" && cmbNivelEstudio.Text != "" && txtNombreUniversidad.Text != "" && txtCarrera.Text != "" && txtCUM.Text != "" && cmbMateriasInscritas.Text != "" && txtNotas.Text != "" && txtNombreProyecto.Text != "" && txtTotalHoras.Text != "" && txtHorasCompletadas.Text != "")
            {
                if (dtpFechaNacimiento.Value.Year >= 2012)
                {
                    //Calculamos el promedio de las notas
                    for (int i = 0; i < contador; i++)
                    {
                        sumaPromedio += notas[i];
                    }
                    double promedio = sumaPromedio / contador;

                    //Invocar método que registrara a los Empleados y los guardara en una lista
                    Estudiante = estudiante.registrarEstudiante(txtDuiEstudiante.Text, txtNombreEstudiante.Text, txtDireccionEstudiante.Text, txtCarnet.Text, cmbNivelEstudio.Text, txtNombreUniversidad.Text, txtCarrera.Text, double.Parse(txtCUM.Text), Int32.Parse(cmbMateriasInscritas.Text), notas, txtNombreProyecto.Text, Int32.Parse(txtTotalHoras.Text), Int32.Parse(txtHorasCompletadas.Text), dtpFechaNacimientoEstudiante.Value, promedio, contador);

                    //Limpiamos las txt.
                    txtNombreEstudiante.Clear();
                    txtDuiEstudiante.Clear();
                    txtDireccionEstudiante.Clear();
                    txtCarnet.Clear();
                    cmbNivelEstudio.Text = "";
                    txtNombreUniversidad.Clear();
                    txtCarrera.Clear();
                    txtCUM.Clear();
                    cmbMateriasInscritas.Text = "";
                    txtNotas.Clear();
                    txtNombreProyecto.Clear();
                    txtTotalHoras.Clear();
                    txtHorasCompletadas.Clear();

                    btnRegistrarEstudiante.Enabled = false;
                    btnRegistrarNota.Enabled = false;
                    txtNotas.Enabled = false;
                    cmbMateriasInscritas.Enabled = true;
                    contador = 0;
                }
                else
                {
                    MessageBox.Show("No es posible registrar estudiantes menores de 9 años");
                }
            }
            else
            {
                MessageBox.Show("Favor no dejar campos vacíos");
            }
        }

        private void cmbMateriasInscritas_SelectedIndexChanged(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro de registrar esta cantidad? \n¡No podrá cambiar la selección!", "Seleccionar cantidad de materias", MessageBoxButtons.YesNo);
            
            if (dialogResult == DialogResult.Yes)
            {
                txtNotas.Enabled = true;
                btnRegistrarNota.Enabled = true;
                cmbMateriasInscritas.Enabled = false;
            }
            else
            {
                cmbMateriasInscritas.Text = "";
            }
        }

        private void btnRegistrarNota_Click(object sender, EventArgs e)
        {
            if (contador < Int32.Parse(cmbMateriasInscritas.Text))
            {
                if (txtNotas.Text != "" && double.Parse(txtNotas.Text) > 0 && double.Parse(txtNotas.Text) <= 10)
                {
                    notas[contador] = double.Parse(txtNotas.Text);
                    MessageBox.Show("Nota " + (contador + 1) + " registrada");
                    contador++;

                    if (contador == Int32.Parse(cmbMateriasInscritas.Text))
                    {
                        btnRegistrarNota.Enabled = false;
                        btnRegistrarEstudiante.Enabled = true;
                        txtNotas.Enabled = false;
                    }
                    else
                    {
                        txtNotas.Clear();
                    }
                }
                else
                {
                    MessageBox.Show("Favor ingresar una nota válida");
                }
            }
        }

        //Sección ver información de usuarios según opción seleccionada por el usuario
        private void btnTodos_Click(object sender, EventArgs e)
        {
            dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Columns.Clear();
            dgvDatosEstudiante.DataSource = Estudiante; /*los nombres de columna que veremos son los de las propiedades*/

            //Orientamos las columas
            dgvDatosEstudiante.Columns["Dui"].DisplayIndex = 0;
            dgvDatosEstudiante.Columns["Nombre"].DisplayIndex = 1;
            dgvDatosEstudiante.Columns["Carnet"].DisplayIndex = 2;
            dgvDatosEstudiante.Columns["Nombreuniversidad"].DisplayIndex = 3;
            dgvDatosEstudiante.Columns["Carrera"].DisplayIndex = 4;
            dgvDatosEstudiante.Columns["CUM"].DisplayIndex = 5;
            dgvDatosEstudiante.Columns["Nombreproyecto"].DisplayIndex = 6;
            dgvDatosEstudiante.Columns["Horascompletadas"].DisplayIndex = 7;
            dgvDatosEstudiante.Columns["Edadpersona"].DisplayIndex = 8;
            //Cambiamos los headers de algunas columnas
            dgvDatosEstudiante.Columns["Nombreuniversidad"].HeaderText = "Nombre universidad";
            dgvDatosEstudiante.Columns["Nombreproyecto"].HeaderText = "Nombre de proyecto";
            dgvDatosEstudiante.Columns["Edadpersona"].HeaderText = "Edad";
            dgvDatosEstudiante.Columns["Horascompletadas"].HeaderText = "Horas pasantías completadas";
            //No mostramos los campos de fecha nacimiento y dirección
            dgvDatosEstudiante.Columns["Fechanac"].Visible = false;
            dgvDatosEstudiante.Columns["Direccasa"].Visible = false;

        }

        private void btnDatosEstudiante_Click(object sender, EventArgs e)
        {
            dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Rows.Clear();
            dgvDatosEstudiante.Columns.Clear();
            dgvDatosEstudiante.Columns.Add("dui", "DUI");
            dgvDatosEstudiante.Columns.Add("nombre", "Nombre");
            dgvDatosEstudiante.Columns.Add("carnet", "Carnet");
            dgvDatosEstudiante.Columns.Add("nivelEstudio", "Nivel de estudio");
            dgvDatosEstudiante.Columns.Add("edadPersona", "Edad");
            //dgvDatosEstudiante.DataSource = Estudiante; /*los nombres de columna que veremos son los de las propiedades*/

            foreach (Ingenieria i in Estudiante)
            {
                int index = dgvDatosEstudiante.Rows.Add();
                dgvDatosEstudiante.Rows[index].Cells[0].Value = i.Dui;
                dgvDatosEstudiante.Rows[index].Cells[1].Value = i.Nombre;
                dgvDatosEstudiante.Rows[index].Cells[2].Value = i.Carnet;
                dgvDatosEstudiante.Rows[index].Cells[3].Value = i.Nivelestudio;
                dgvDatosEstudiante.Rows[index].Cells[4].Value = i.Edadpersona;
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Columns.Clear();
        }

        private void btnRegistroUniversitario_Click(object sender, EventArgs e)
        {
            dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Rows.Clear();
            dgvDatosEstudiante.Columns.Clear();
            dgvDatosEstudiante.Columns.Add("dui", "DUI");
            dgvDatosEstudiante.Columns.Add("nombre", "Nombre");
            dgvDatosEstudiante.Columns.Add("carnet", "Carnet");
            dgvDatosEstudiante.Columns.Add("nombreUniversidad", "Universidad");
            dgvDatosEstudiante.Columns.Add("carrera", "Carrera");
            dgvDatosEstudiante.Columns.Add("materiasInscritas", "Materias inscritas");
            dgvDatosEstudiante.Columns.Add("cum", "CUM");

            foreach (Ingenieria i in Estudiante)
            {
                int index = dgvDatosEstudiante.Rows.Add();
                dgvDatosEstudiante.Rows[index].Cells[0].Value = i.Dui;
                dgvDatosEstudiante.Rows[index].Cells[1].Value = i.Nombre;
                dgvDatosEstudiante.Rows[index].Cells[2].Value = i.Carnet;
                dgvDatosEstudiante.Rows[index].Cells[3].Value = i.Nombreuniversidad;
                dgvDatosEstudiante.Rows[index].Cells[4].Value = i.Carrera;
                dgvDatosEstudiante.Rows[index].Cells[5].Value = i.Materiasinscritas;
                dgvDatosEstudiante.Rows[index].Cells[6].Value = i.CUM;
            }
        }

        private void btnRegistroPasantias_Click(object sender, EventArgs e)
        {
            dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Rows.Clear();
            dgvDatosEstudiante.Columns.Clear();
            dgvDatosEstudiante.Columns.Add("nombre", "Nombre");
            dgvDatosEstudiante.Columns.Add("carnet", "Carnet");
            dgvDatosEstudiante.Columns.Add("nombreProyecto", "Nombre proyecto");
            dgvDatosEstudiante.Columns.Add("totalHoras", "Total horas");
            dgvDatosEstudiante.Columns.Add("horasCompletas", "# horas completas");

            foreach (Ingenieria i in Estudiante)
            {
                int index = dgvDatosEstudiante.Rows.Add();
                dgvDatosEstudiante.Rows[index].Cells[0].Value = i.Nombre;
                dgvDatosEstudiante.Rows[index].Cells[1].Value = i.Carnet;
                dgvDatosEstudiante.Rows[index].Cells[2].Value = i.Nombreproyecto;
                dgvDatosEstudiante.Rows[index].Cells[3].Value = i.Totalhoras;
                dgvDatosEstudiante.Rows[index].Cells[4].Value = i.Horascompletadas;
            }
        }

        private void btnPromedio_Click(object sender, EventArgs e)
        {
            dgvDatosEstudiante.DataSource = null;
            dgvDatosEstudiante.Rows.Clear();
            dgvDatosEstudiante.Columns.Clear();
            dgvDatosEstudiante.Columns.Add("nombre", "Nombre");
            dgvDatosEstudiante.Columns.Add("carnet", "Carnet");
            dgvDatosEstudiante.Columns.Add("promedio", "Promedio");

            foreach (Ingenieria i in Estudiante)
            {
                int index = dgvDatosEstudiante.Rows.Add();
                dgvDatosEstudiante.Rows[index].Cells[0].Value = i.Nombre;
                dgvDatosEstudiante.Rows[index].Cells[1].Value = i.Carnet;
                dgvDatosEstudiante.Rows[index].Cells[2].Value = i.Promedio;
            }
        }
    }
}
