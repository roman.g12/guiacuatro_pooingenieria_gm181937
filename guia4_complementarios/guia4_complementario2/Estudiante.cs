﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia4_complementario2
{
    class Estudiante : Persona
    {
        private string carnet;
        private string nivelestudio;

        public Estudiante()
        {
            
        }

        public string Carnet { get => carnet; set => carnet = value; }
        public string Nivelestudio { get => nivelestudio; set => nivelestudio = value; }
    }
}
