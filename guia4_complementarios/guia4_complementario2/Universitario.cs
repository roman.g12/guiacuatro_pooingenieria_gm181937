﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guia4_complementario2
{
    class Universitario : Estudiante
    {
        string nombreuniversidad;
        string carrera;
        int materiasinscritas;
        double[] notas = new double[4];
        double cum;
        double promedio;

        public Universitario()
        {

        }

        public string Nombreuniversidad { get => nombreuniversidad; set => nombreuniversidad = value; }
        public string Carrera { get => carrera; set => carrera = value; }
        public int Materiasinscritas { get => materiasinscritas; set => materiasinscritas = value; }
        public double[] Notas { get => notas; set => notas = value; }
        public double CUM { get => cum; set => cum = value; }
        public double Promedio { get => promedio; set => promedio = value; }
    }
}
